require('./index.css');
import React                 from "react";
import ReactDOM              from "react-dom";
import Editor                from './components/editor';
import CkEditor              from './components/ckedit';
import SourceEditor          from './components/monaco';
import Toolbar               from './components/toolbar';
import debug                 from 'debug';
import modes                 from './lib/modes';
import events                from './lib/events';
import commands              from './lib/commands';
import Command               from './lib/commands/switchEditModeCommand';
import bus                   from './lib/eventBus';
import YoutubeWidget         from './components/contentWidgets/youtubeVideo';
import ContentExpanderWidget from './components/contentWidgets/contentExpander';

const log = debug('main');


class Index extends React.Component {
    constructor() {
        super();
        this.contentWidgets = [
            YoutubeWidget,
            ContentExpanderWidget
        ];

        this.val = `
            <div class="main-grid" style="display: grid; grid-template-columns: 49% 49%;grid-template-rows: auto;">
                <p class="text-block">
                    hello im <b>block 1</b>
                </p>
                <p class="text-block">
                    hello im <i>block 2</i>
                </p>
                <p class="text-block">
                    hello im <i>block 3</i>
                </p>
                <p class="text-block">
                    hello im <i>block 4</i>
                </p>
            </div>
            <br/>
            <input type="button" value="do it!" />
        `;
    }

    render() {
        return (
            <div>
                <Editor value={this.val} contentWidgets={this.contentWidgets}/>
            </div>);
    }
};

/*
<youtube-video url="https://www.youtube.com/watch?v=X4MOLgIobs0"></youtube-video>
 */

ReactDOM.render(
    <React.StrictMode>

        <Index/>
    </React.StrictMode>, document.getElementById("index"));