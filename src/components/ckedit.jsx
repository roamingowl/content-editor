import React          from "react";
import PropTypes      from "prop-types";
import ReactDOM       from "react-dom";
import scriptLoader   from './../lib/scriptLoader';
import debug          from 'debug';
import bus            from '../lib/eventBus';
import ContentCommand from './../lib/commands/contentCommand';
import camelCase      from 'camelcase';

const log = debug('react-ckeditor');

const defaultScriptUrl = "https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js";

export default class CKEditor extends React.Component {
    constructor(props) {
        super(props);

        this.changeThrottleTimer = null;

        this.preventchangeEvent = false;

        this.value = this.props.value;

        this.mainDiv = React.createRef();

        this.state = {
            isScriptLoaded: this.props.isScriptLoaded,
        };
    }

    //load ckeditor script as soon as component mounts if not already loaded
    async componentDidMount() {
        await scriptLoader.ensureLoaded(defaultScriptUrl);
        this.onLoad();
        bus.on(ContentCommand.EVENT_NAME, this.onCommand);
    }

    onCommand = (evt) => {
        if (this.editorInstance) {
            log('Available commands: ', this.editorInstance.commands);
            log(`Executing cmd ${evt.cmd}, options: `, evt.options);
            this.editorInstance.execCommand(evt.cmd);
        }
    };

    shouldComponentUpdate(nextProps, nextState) {
        log('shouldComponentUpdate', nextProps.value !== this.value);
        return nextProps.value !== this.value;
    }

    componentDidUpdate(prevProps) {
        log('componentDidUpdate ', this.value);
        // Always refer to the latest value
        //this.__current_value = this.props.value;
        // Consider the situation of rendering 1+ times before the editor mounted
        if (this.editorInstance) {
            if (this.editorInstance.getData() !== this.props.value) {
                log('setting value', this.props.value);
                this.preventchangeEvent = true;
                this.editorInstance.setData(this.props.value);
                this.preventchangeEvent = false;
            }
        }
    }

    initEditor = (evt) => {
        log('Clicked ', evt.target);
        this.props.onWidgetFocused(evt.target);
        evt.target.contentEditable = true;
        let blocks                 = this.mainDiv.current.querySelectorAll('.text-block');
        for (let i = 0; i < blocks.length; i++) {
            if (blocks[i] === evt.target) {
                continue;
            }
            blocks[i].contentEditable = false;
        }
    };

    componentDidMount() {
        let blocks = this.mainDiv.current.querySelectorAll('.text-block');
        for (let i = 0; i < blocks.length; i++) {
            blocks[i].addEventListener('click', this.initEditor, {once: true});
        }
    }

    componentWillUnmount() {
        this.unmounting = true;
        bus.off(ContentCommand.EVENT_NAME, this.onCommand);
        this.editorInstance.destroy();
    }

    onLoad = () => {
        if (this.unmounting) return;

        this.setState({
            isScriptLoaded: true
        });

        if (!window.CKEDITOR) {
            console.error("CKEditor not found");
            return;
        }

        if (this.props.contentWidgets) {
            this.registerCustomElements(this.props.contentWidgets);
        }

        window.CKEDITOR.disableAutoInline = true;

        // window.CKEDITOR.on('instanceReady', (evt) =>{
        //     log('i_ready', evt.editor.window.$.customElements)
        //     try {
        //         evt.editor.window.$.customElements.define('youtube-video', YoutubeVideo);
        //     } catch (e) {
        //         //nono
        //     }
        // });

        if (this.props.contentWidgets) {
            this.registerWidgets(this.props.contentWidgets);
        }

        let editorConfig = Object.assign({}, this.props.config, {
            extraPlugins: this.props.config.extraPlugins.split(',').concat(this.getWidgetPluginNames(this.props.contentWidgets)).join(',')
        });

        log('Editor configuration ', editorConfig);

        this.editorInstance = window.CKEDITOR.inline(
            ReactDOM.findDOMNode(this),
            editorConfig
        );


        this.editorInstance.on('change', (e) => {
            if (this.preventchangeEvent) {
                return;
            }
            this.value = e.editor.getData();
            if (this.changeThrottleTimer) {
                clearTimeout(this.changeThrottleTimer);
            }
            this.changeThrottleTimer = setTimeout(() => {
                this.props.onChange(e.editor.getData());
            }, 500)
        });
    };

    /**
     *
     * @param contentWidgets
     * @return {Array}
     */
    getWidgetPluginNames = (contentWidgets) => {
        let pluginNames = [];
        for (let i = 0; i < contentWidgets.length; i++) {
            pluginNames.push(contentWidgets[i].id);
        }
        return pluginNames;
    };

    createMarkup = () => {
        return {__html: this.value};
    };

    registerWidgets = (contentWidgets) => {
        for (let i = 0; i < contentWidgets.length; i++) {
            let widget = contentWidgets[i];
            if (CKEDITOR.plugins.list && CKEDITOR.plugins.registered && CKEDITOR.plugins.registered[widget.id]) {
                log(`Widget ${widget.customElementClass.name} already registered`);
                return;
            }
            CKEDITOR.plugins.add(widget.id, {
                requires: 'widget',
                init:     function (editor) {
                    editor.widgets.add(widget.id, {
                        allowedContent:  true,
                        requiredContent: `youtube-video(${widget.id})`,
                        upcast:          function (element) {
                            log('upcats cb ', element);
                            return element.name === widget.customElementClass.name && element.hasClass(widget.id);
                        },
                        template:        widget.template || `<${widget.customElementClass.name} class="${widget.id}"></${widget.customElementClass.name}>`,
                        editables:       widget.editables,
                        data:            function () {
                            log('data cd!', this.data);
                            let model   = this.data.model;
                            let element = this.element;

                            if (model) {
                                element.$.dataset.model = JSON.stringify(model);
                                widget.onDataChanged(element, model);
                            }
                        },
                        edit:            function () {
                            log('widget edit cb');
                            widget.onEdit(this);
                        }
                    });
                    editor.addCommand(camelCase(`add-${widget.customElementClass.name}`), {
                        exec: () => {
                            widget.onAdd(editor);
                        }
                    });
                },

            });
        }
    };

    registerCustomElements = (contentWidgets) => {
        contentWidgets.map((widget) => {
            if (!customElements.get(widget.customElementClass.name)) {
                log(`Registering custom element ${widget.customElementClass.name}`)
                customElements.define(widget.customElementClass.name, widget.customElementClass);
            }
        })
    };

    render() {
        return <div ref={this.mainDiv} style={{border: '1px solid lightGray', padding: '.5em'}}
                    className="content-editor"
            //contentEditable={true}
                    dangerouslySetInnerHTML={this.createMarkup()}/>;
    }
}

CKEditor.defaultProps = {
    value:          "",
    config:         {
        toolbarStartupExpanded: false,
        removePlugins:          'sourcearea',
        extraPlugins:           '',
        allowedContent:         true,
        startupFocus:           true
    },
    onChnage:       null,
    contentWidgets: []
};

CKEditor.propTypes = {
    value:          PropTypes.string,
    config:         PropTypes.object,
    onChange:       PropTypes.func,
    contentWidgets: PropTypes.array
};