import React        from "react";
import ReactDOM     from "react-dom";
import CkEditor     from './ckedit';
import SourceEditor from './monaco';
import Toolbar      from './toolbar';
import debug        from 'debug';
import modes        from './../lib/modes';
import Command      from './../lib/commands/switchEditModeCommand';
import bus          from './../lib/eventBus';
import Preview      from './preview';
import DocumentEdit from "./document";

const log = debug('main');

export default class Editor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mode:  modes.CONTENT,
            value: this.props.value || ''
        }
    }

    componentWillUnmount() {
        bus.off(Command.EVENT_NAME, this.onEditModeSwitched)
    };

    componentDidMount() {
        bus.on(Command.EVENT_NAME, this.onEditModeSwitched)
    };

    onEditModeSwitched = (evt) => {
        this.setState({mode: evt.mode});
    };

    onChange = (newValue) => {
        this.setState({value: newValue});
    };

    onWidgetFocused = (widget) => {
        log(`Focused on widget `, widget);
    }

    //<CkEditor value={this.state.value} contentWidgets={this.props.contentWidgets} onChange={this.onChange} onWidgetFocused={this.onWidgetFocused}/>
    render() {
        return <div>
            <Toolbar mode={this.state.mode}/>
            {this.state.mode === modes.CONTENT &&
            <DocumentEdit value={this.state.value}/>
            }
            {this.state.mode === modes.SOURCE &&
            <SourceEditor value={this.state.value} onChange={this.onChange}/>
            }
            {this.state.mode === modes.PREVIEW &&
            <Preview value={this.state.value}/>
            }
        </div>;
    }
};