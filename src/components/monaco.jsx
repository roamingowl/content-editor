import React        from "react";
import monacoLoader from './../lib/monacoLoader';
import PropTypes    from "prop-types";
import debug        from 'debug';

const log = debug('react-monaco');

export default class SourceEditor extends React.Component {
    constructor(props) {
        super(props);
        this.srcPath = 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.13.1/min';
        this.editor;

        this.latestValue        = this.props.value;
        this.preventchangeEvent = false;

        this.editorOptions = {
            language: 'html',
            height:   200,
            value:    this.props.value
        };

        // this.state = {
        //     code: '// type your code...',
        // }

        this.editorDiv = React.createRef();
    }

    editorHasLoaded = (editor, monaco) => {
        this.editor = editor;
        this.monaco = monaco;
        // this.editor.addCommand([monaco.KeyMod.CtrlCmd | monaco.KeyCode.KEY_Z], () => {
        //     log('calling undo!');
        //     this.editor.getModel().undo();
        // });
        // this.editor.addCommand([monaco.KeyMod.CtrlCmd | monaco.KeyCode.KEY_Z], () => {
        //     this.editor.getModel().undo();
        // });
        this.editor.onDidChangeModelContent(event =>
            this.onChange(editor, event)
        );
    };

    componentDidUpdate(prevProps) {
        if (this.latestValue !== this.props.value) {
            // Always refer to the latest value
            //this.__current_value = this.props.value;
            // Consider the situation of rendering 1+ times before the editor mounted
            if (this.editor) {
                this.preventchangeEvent = true;
                this.editor.setValue(this.props.value);
                this.preventchangeEvent = false;
                this.latestValue = this.props.value;
            }
        }
        // if (prevProps.language !== this.props.language) {
        //     monaco.editor.setModelLanguage(this.editor.getModel(), this.props.language);
        // }
        // if (prevProps.theme !== this.props.theme) {
        //     monaco.editor.setTheme(this.props.theme);
        // }
        // if (
        //     this.editor &&
        //     (this.props.width !== prevProps.width || this.props.height !== prevProps.height)
        // ) {
        //     this.editor.layout();
        // }
    }

    afterEditorSrcLoaded = () => {
        this.editor = window.monaco.editor.create(this.editorDiv.current, this.editorOptions);
        this.editorHasLoaded(this.editor, window.monaco);
    };

    componentDidMount() {
        monacoLoader.load(this.srcPath, this.afterEditorSrcLoaded);
    }

    editorDidMount = (editor, monaco) => {
        this.editor = editor;
    };

    onChange = (editor) => {
        if (this.preventchangeEvent) {
            return;
        }
        if (this.props.onChange) {
            this.props.onChange(editor.getValue());
        }
        //this.setState({code: editor.value});
    };

    render() {
        return (
            <div style={{height: 200}} ref={this.editorDiv}></div>
        );
    }
}

SourceEditor.defaultProps = {
    value:    '',
    onChnage: null
};

SourceEditor.propTypes = {
    value:    PropTypes.string,
    onChnage: PropTypes.func
};