import React          from "react";
import PropTypes      from "prop-types";
import ReactDOM       from "react-dom";
import debug          from 'debug';
import bus            from '../lib/eventBus';
import ContentCommand from './../lib/commands/contentCommand';

const log = debug('GridDocument');

export default class GridDocument extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            grid: {
                rows:     0,
                cols:     0,
                rowItems: [],
            }
        }
    }

    markupToModel = (html) => {
        let div = document.createElement('div');
        div.style.display = 'none';
        div.innerHTML = html;

        div.

        //TODO
        return {
            rows:     0,
            cols:     0,
            rowItems: []
        }
    }

    modelToMarkup = (html) => {
        //?
    }

    addRow = () => {

    }

    addColumn = () => {

    }

    removeRow = () => {

    }

    removeColumn = () => {

    }

    createMarkup = () => {
        return {__html: this.props.value};
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            grid: this.markupToModel(newProps)
        })
    }

    componentWillMount() {
        this.setState({
            grid: this.markupToModel(this.props.value)
        })
    }

    render() {
        return {
        <div ref={this.mainDiv}
             dangerouslySetInnerHTML={this.createMarkup()}/>;
    }
    }
}