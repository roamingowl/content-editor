import React            from 'react';
import ReactDOM         from "react-dom";
import youtubeThumbnail from "youtube-thumbnail";

export default class YoutubeVideo extends HTMLElement {
    static get name() {
        return 'youtube-video'
    }
    static get observedAttributes() {
        return ['url'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'url':
                console.log(`URL changed from ${oldValue} to ${newValue}`);
                if (!this.shadowRoot) {
                    return;
                }
                let image = this.shadowRoot.querySelector('img');
                let thumbUrl = youtubeThumbnail(newValue);
                image.src = thumbUrl.default.url;
                break;
        }
    }

    connectedCallback() {
        const mountPoint = document.createElement('div');
        this.attachShadow({mode: 'open'}).appendChild(mountPoint);
        const url = this.getAttribute('url');

        let thumbUrl = youtubeThumbnail(url);
        console.log('Yurl: ', thumbUrl);

        ReactDOM.render(<img src={thumbUrl.default.url} width={thumbUrl.default.width}
                             height={thumbUrl.default.height}/>, mountPoint);
    }
}