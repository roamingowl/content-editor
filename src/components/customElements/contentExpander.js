import React    from 'react';
import ReactDOM from "react-dom";

class ReactContentExpander extends React.Component {
    constructor(props) {
        super(props);
        this.label = React.createRef();
        this.state = {
            expanded: this.props.editMode === true ? true : false
        }
    }

    componentDidMount() {
        console.log('label', this.label);
        this.label.current.addEventListener('click', this.expand);
    }

    componentWillUnmount() {
        this.label.current.removeEventListener('click', this.expand);
    }

    expand = () => {
        console.log('click!');
        this.setState({expanded: !this.state.expanded});
    };

    render() {
        return (<div>
            <div ref={this.label} className="content-expander-label">{this.props.label}</div>
            <br/>
            {this.state.expanded &&
            <slot name="content">Content is empty</slot>
            }
        </div>)

    }
}

export default class ContentExpander extends HTMLElement {
    static get name() {
        return 'content-expander'
    }

    static get observedAttributes() {
        return ['label'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'label':
                console.log(`Label changed from ${oldValue} to ${newValue}`);
                if (!this.shadowRoot) {
                    return;
                }
                let label       = this.shadowRoot.querySelector('.content-expander-label');
                label.innerHTML = newValue;
                break;
        }
    }

    inEditMode(el, cls) {
        while (el.parentElement) {
            if (el.classList.contains(cls)) {
                return true;
            }
            el = el.parentElement;
        }
        return false;
    }

    connectedCallback() {
        const mountPoint = document.createElement('div');
        this.attachShadow({mode: 'open'}).appendChild(mountPoint);
        const label = this.getAttribute('label');

        ReactDOM.render(<ReactContentExpander label={label}
                                              editMode={this.inEditMode(this, 'content-editor')}></ReactContentExpander>, mountPoint);
    }
}