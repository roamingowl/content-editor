import React          from "react";
import PropTypes      from "prop-types";
import ReactDOM       from "react-dom";
import debug          from 'debug';
import bus            from '../lib/eventBus';
import scriptLoader   from './../lib/scriptLoader';
import ContentCommand from './../lib/commands/contentCommand';

const log = debug('DocumentEdit');

const defaultScriptUrl = "https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js";

export default class DocumentEdit extends React.Component {
    constructor(props) {
        super(props);
        this.mainDiv = React.createRef();

    }

    //load ckeditor script as soon as component mounts if not already loaded

    clearEditors = (evt) => {
        if (this.mainDiv.current === evt.target) {
            log('Clear editors', evt.target, evt.currentTarget);
            this.removeEditorInstance();
        }
    }

    removeEditorInstance(currentTarget) {
        if (this.editorInstance) {
            let blocks = this.mainDiv.current.querySelectorAll('.text-block');
            log('closing other blocks', blocks);
            for (let i = 0; i < blocks.length; i++) {
                if (currentTarget && blocks[i] === currentTarget) {
                    continue;
                }
                blocks[i].contentEditable = false;
                blocks[i].addEventListener('click', this.initEditor, {once: true});
            }
            this.editorInstance.destroy();
            delete this.editorInstance;
            log('old editor destroyed');
        }
    }

    initEditor = (evt) => {
        log('Clicked ', evt.currentTarget);

        let editorConfig = Object.assign({}, this.props.config, {
            //extraPlugins: this.props.config.extraPlugins.split(',').concat(this.getWidgetPluginNames(this.props.contentWidgets)).join(',')
        });
        evt.currentTarget.contentEditable = true;
        this.editorInstance = window.CKEDITOR.inline(
            evt.currentTarget,
            editorConfig
        );
        // evt.currentTarget.addEventListener('blur', () => {
        //     evt.currentTarget.contentEditable = false;
        //     log('Lost focus');
        //     evt.currentTarget.addEventListener('click', this.initEditor, {once: true});
        // }, {once: true});
    };

    async componentDidMount() {
        let blocks = this.mainDiv.current.querySelectorAll('.text-block');
        for (let i = 0; i < blocks.length; i++) {
            blocks[i].addEventListener('click', this.initEditor, {once: true});
        }
        await scriptLoader.ensureLoaded(defaultScriptUrl);
        bus.on(ContentCommand.EVENT_NAME, this.onCommand);
        window.CKEDITOR.disableAutoInline = true;
        this.mainDiv.current.addEventListener('click', this.clearEditors);
    }

    createMarkup = () => {
        return {__html: this.props.value};
    };

    onCommand = (evt) => {
        if (this.editorInstance) {
            log('Available commands: ', this.editorInstance.commands);
            log(`Executing cmd ${evt.cmd}, options: `, evt.options);
            this.editorInstance.execCommand(evt.cmd);
        }
    };

    render() {
        return <div ref={this.mainDiv} style={{border: '1px solid lightGray', padding: '.5em'}}
                    className="DocumentEdit"
                    dangerouslySetInnerHTML={this.createMarkup()}/>;
    }
}

DocumentEdit.defaultProps = {
    value:          "",
    config:         {
        toolbarStartupExpanded: false,
        removePlugins:          'sourcearea',
        extraPlugins:           '',
        allowedContent:         true,
        startupFocus:           true
    },
    onChnage:       null,
    contentWidgets: []
};

DocumentEdit.propTypes = {
    value:          PropTypes.string,
    config:         PropTypes.object,
    onChange:       PropTypes.func,
    contentWidgets: PropTypes.array
};