import React          from 'react';
import styled         from 'styled-components';
import bus            from '../lib/eventBus';
import commands       from '../lib/commands';
import events         from '../lib/events';
import modes          from '../lib/modes';
import SwitchCommand  from './../lib/commands/switchEditModeCommand';
import ContentCommand from './../lib/commands/contentCommand';

const MainDiv = styled.div`
  display: flex;
`;

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    runContentCmd(cmd) {
        bus.emit(events.CONTENT_COMMAND, cmd);
    }

    getSwitchModeName() {
        if (this.props.mode === modes.SOURCE) {
            return 'Show content';
        }
        return 'Show source';
    }

    render() {
        return <MainDiv>
            <button onClick={() => {
                new SwitchCommand.SwitchEditModeCommand(modes.SOURCE).run()
            }}>Source
            </button>
            <button onClick={() => {
                new SwitchCommand.SwitchEditModeCommand(modes.CONTENT).run()
            }}>Content
            </button>
            <button onClick={() => {
                new SwitchCommand.SwitchEditModeCommand(modes.PREVIEW).run()
            }}>Preview
            </button>
            <button onClick={() => {
                new ContentCommand.ContentCommand(commands.content.BOLD).run()
            }}>bold
            </button>
            <button onClick={() => {
                new ContentCommand.ContentCommand('addYoutubeVideo').run()
            }}>ytb
            </button>
            <button onClick={() => {
                new ContentCommand.ContentCommand('addContentExpander').run()
            }}>expander
            </button>
        </MainDiv>
    }
}