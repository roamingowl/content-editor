import React from 'react';

export default class Preview extends React.Component {
    constructor(props) {
        super(props);

        //??
    }

    createMarkup = () => {
        return {__html: this.props.value};
    };

    render() {
        return (
            <div dangerouslySetInnerHTML={this.createMarkup()}></div>
        )
    }
}