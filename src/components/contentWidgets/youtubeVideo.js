import YoutubeWidget from '../customElements/youtubeVideo';
import debug         from 'debug';
import camelCase     from 'camelcase';

const log = debug('youtube-custom-element');

const MODEL_FIELD_NAME = 'model';

const WIDGET_ID = camelCase(YoutubeWidget.name);

export default {
    customElementClass: YoutubeWidget,
    id:           WIDGET_ID,
    onAdd:              async (editor) => {
        log('Adding widget ', YoutubeWidget.name);
        let url   = window.prompt('Enter youtube video url');
        let model = {
            url,
            add: true
        };

        let startupData = {
            [MODEL_FIELD_NAME]: model,
        }

        log('Exec ckeditor cmd', WIDGET_ID, startupData);
        editor.execCommand(WIDGET_ID, {
            startupData
        });

    },
    onDataChanged: (element, model) =>{
        element.$.setAttribute('url', model.url);
    },
    onEdit:             (widget) => {
        log('Editing widget', widget.data.model);
        let model = widget.data.model;
        if (model && model.add) {
            delete model.add;
            widget.setData(MODEL_FIELD_NAME, model);
            return;
        }
        if (!model) {
            log('No model!', widget, this);
            model = JSON.parse(widget.element.$.dataset.model);
        }
        let url = window.prompt('Edit youtube video url', model.url);
        if (!url) {
            return;
        }
        model.url = url;
        log(widget);
        log('Setting new widget data', MODEL_FIELD_NAME, model);
        widget.setData(MODEL_FIELD_NAME, model);
        widget.setData('updated_at', new Date());
    }
}