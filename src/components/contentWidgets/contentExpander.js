import ContentExpanderWidget from '../customElements/contentExpander';
import debug                 from 'debug';
import camelCase             from 'camelcase';

const log = debug('content-expander-custom-element');

const MODEL_FIELD_NAME = 'model';

const WIDGET_ID = camelCase(ContentExpanderWidget.name);

export default {
    customElementClass: ContentExpanderWidget,
    id:                 WIDGET_ID,
    onAdd:              async (editor) => {
        log('Adding widget ', ContentExpanderWidget.name);
        let label = window.prompt('Enter label');
        let model = {
            label,
            add: true
        };

        let startupData = {
            [MODEL_FIELD_NAME]: model,
        }

        log('Exec ckeditor cmd', WIDGET_ID, startupData);
        editor.execCommand(WIDGET_ID, {
            startupData
        });

    },
    editables:          {
        content: {
            selector:       '.editable-content',
            //allowedContent: true
        }
    },
    template:           `<${ContentExpanderWidget.name} class="${WIDGET_ID}"><div class="editable-content" slot="content"></div></${ContentExpanderWidget.name}>`,
    onDataChanged:      (element, model) => {
        element.$.setAttribute('label', model.label);
    },
    onEdit:             (widget) => {
        log('Editing widget', widget.data.model);
        let model = widget.data.model;
        if (model && model.add) {
            delete model.add;
            widget.setData(MODEL_FIELD_NAME, model);
            return;
        }
        if (!model) {
            log('No model!', widget, this);
            model = JSON.parse(widget.element.$.dataset.model);
        }
        let label = window.prompt('Edit label', model.label);
        if (!label) {
            return;
        }
        model.label = label;
        log('Setting new widget data', MODEL_FIELD_NAME, model);
        widget.setData(MODEL_FIELD_NAME, model);
        widget.setData('updated_at', new Date());
    }
}