import BaseCommand from './base';

const EVENT_NAME = 'switch-edit-mode';

class SwitchEditModeCommand extends BaseCommand {
    static getEventName() {
        return EVENT_NAME;
    }

    constructor(mode) {
        super();
        this.eventName = EVENT_NAME;
        this.options   = {
            mode
        }
    }
}

export default {
    EVENT_NAME,
    SwitchEditModeCommand
}