import bus from '../eventBus';

export default class {
    run() {
        bus.emit(this.eventName, this.options);
    }
}