import BaseCommand from './base';

const EVENT_NAME = 'content-command';

class ContentCommand extends BaseCommand {
    static getEventName() {
        return EVENT_NAME;
    }

    constructor(cmd, options = {}) {
        super();
        this.eventName = EVENT_NAME;
        this.options   = {
            cmd,
            options
        }
    }
}

export default {
    EVENT_NAME,
    ContentCommand
}