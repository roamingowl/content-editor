const NAME = 'switch-mode-event';

class SwitchModeEvent {
    constructor(mode) {
        this.name =
            this.mode = mode;
    }
}

export default {
    NAME,
    get(mode) {
        return new SwitchModeEvent(mode);
    }
}
