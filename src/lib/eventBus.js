import debug from 'debug';

const EventEmitter = require('events');
const log          = debug('emitter');

class Emitter extends EventEmitter {
    off() {
        this.removeListener(...arguments);
    }

    emit(name, ...args) {
        log(`Emitting event ${name}`, ...args);
        super.emit(name, ...args);
    }
}

export default new Emitter();